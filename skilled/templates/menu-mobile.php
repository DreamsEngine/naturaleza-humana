<?php
$theme_location = 'primary_navigation';

if ( has_nav_menu( 'mobile_navigation' ) ) {
	$theme_location = 'mobile_navigation';
}

$defaults = array(
	'theme_location'  => $theme_location,
	'menu_class'      => skilled_class( 'respmenu' ),
	// 'container_class' => skilled_class( 'main-menu-container' ),
	'depth'           => 3,
	'fallback_cb'     => false,
	'walker'          => new Skilled_Mobile_Menu_Walker()
);

$logo     = skilled_get_option( 'respmenu-logo', array() );
$logo_url = isset( $logo['url'] ) && $logo['url'] ? $logo['url'] : '';
$logo_alt_text = skilled_get_option('logo-alt-text', 'logo');

if ( ! $logo_url ) {
	$logo     = skilled_get_option( 'logo', array() );
	$logo_url = isset( $logo['url'] ) && $logo['url'] ? $logo['url'] : '';
}

$respmenu_display_switch     = skilled_get_option( 'respmenu-display-switch-img', array() );
$respmenu_display_switch_url = isset( $respmenu_display_switch['url'] ) && $respmenu_display_switch['url'] ? $respmenu_display_switch['url'] : '';

?>
<div id="wh-mobile-menu" class="respmenu-wrap">
	<div class="respmenu-header">
		<?php if ($logo_url) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="respmenu-header-logo-link">
				<img src="<?php echo esc_url( $logo_url ); ?>" class="respmenu-header-logo" alt="<?php echo esc_attr(trim($logo_alt_text)); ?>">
			</a>
		<?php endif; ?>
		<div class="respmenu-open">
		<?php if ($respmenu_display_switch_url) : ?>
			<img src="<?php echo esc_url( $respmenu_display_switch_url ); ?>" alt="mobile-menu-display-switch">
		<?php else: ?>
			<hr><hr><hr>
		<?php endif; ?>
		</div>
	</div>
	<?php wp_nav_menu( $defaults ); ?>
</div>
