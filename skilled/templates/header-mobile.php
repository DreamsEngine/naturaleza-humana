<?php
$mobile_header_layout_block = skilled_get_layout_block( 'header-layout-block-mobile' );
// dd($mobile_header_layout_block);
?>
<div class="<?php echo skilled_class( 'header-mobile', array('layout_block' => $mobile_header_layout_block) ); ?>">
	<?php if ( $mobile_header_layout_block ): ?>
		<?php echo do_shortcode( $mobile_header_layout_block->post_content ); ?>
	<?php else: ?>
		<?php get_template_part('templates/menu-mobile'); ?>
	<?php endif; ?>
</div>