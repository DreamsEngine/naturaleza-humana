<?php
/**
 * Custom functions
 */

add_action( 'wp_head', 'skilled_custom_css' );
add_action( 'wp_head', 'skilled_custom_js_code' );
add_action( 'wp_head', 'skilled_google_analytics_code' );
add_action( 'wp_head', 'skilled_responsive_menu_scripts' );
add_action( 'wp_head', 'skilled_add_layout_blocks_css' );

add_filter( 'wp_nav_menu_items', 'skilled_wcmenucart', 10, 2 );

add_filter( 'msm_filter_menu_location', 'skilled_msm_filter_menu_location' );
add_filter( 'msm_filter_load_compiled_style', 'skilled_msm_filter_load_compiled_style' );

add_filter( 'breadcrumb_trail_labels', 'skilled_breadcrumb_trail_labels' );

function skilled_add_layout_blocks_css() {

	$header_layout_block_id = skilled_get_layout_block_id( 'header-layout-block' );
	echo skilled_get_vc_page_custom_css( $header_layout_block_id );
	echo skilled_get_vc_shortcodes_custom_css( $header_layout_block_id );

	$mobile_header_layout_block_id = skilled_get_layout_block_id( 'header-layout-block-mobile' );
	echo skilled_get_vc_page_custom_css( $mobile_header_layout_block_id );
	echo skilled_get_vc_shortcodes_custom_css( $mobile_header_layout_block_id );

	$footer_layout_block_id = skilled_get_layout_block_id( 'footer-layout-block' );
	echo skilled_get_vc_page_custom_css( $footer_layout_block_id );
	echo skilled_get_vc_shortcodes_custom_css( $footer_layout_block_id );

	$quick_sidebar_layout_block_id = skilled_get_layout_block_id( 'quick-sidebar-layout-block' );
	echo skilled_get_vc_page_custom_css( $quick_sidebar_layout_block_id );
	echo skilled_get_vc_shortcodes_custom_css( $quick_sidebar_layout_block_id );
}


function skilled_filter_body_class( $body_classes ) {

	$body_classes[] = $class = 'header-' . skilled_get_option( 'header-location', 'top' );

	return $body_classes;
}

function skilled_msm_filter_menu_location( $menu_location ) {
	global $post_id;
	$use_custom_menu_location = skilled_get_rwmb_meta( 'use_custom_menu', $post_id );
	if ( $use_custom_menu_location ) {
		$custom_menu_location = skilled_get_rwmb_meta( 'custom_menu_location', $post_id );
		if ( ! empty( $custom_menu_location ) ) {
			return $custom_menu_location;
		}
	}

	return $menu_location;
}

function skilled_msm_filter_load_compiled_style() {
	return false;
}

function skilled_get_vc_page_custom_css( $id ) {

	$out = '';
	if ( $id ) {
		$post_custom_css = get_post_meta( $id, '_wpb_post_custom_css', true );
		if ( ! empty( $post_custom_css ) ) {
			$post_custom_css = strip_tags( $post_custom_css );
			$out .= '<style type="text/css" data-type="vc_custom-css">';
			$out .= $post_custom_css;
			$out .= '</style>';
		}
	}

	return $out;
}

function skilled_get_vc_shortcodes_custom_css( $id ) {

	$out = '';
	if ( $id ) {
		$shortcodes_custom_css = get_post_meta( $id, '_wpb_shortcodes_custom_css', true );
		if ( ! empty( $shortcodes_custom_css ) ) {
			$shortcodes_custom_css = strip_tags( $shortcodes_custom_css );
			$out .= '<style type="text/css" data-type="vc_shortcodes-custom-css">';
			$out .= $shortcodes_custom_css;
			$out .= '</style>';
		}
	}

	return $out;
}

function skilled_is_sensei_active() {

	$active_plugins = (array) get_option( 'active_plugins', array() );

	if ( is_multisite() ) {

		$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
	}

	return in_array( 'woothemes-sensei/woothemes-sensei.php', $active_plugins )
	       || array_key_exists( 'woothemes-sensei/woothemes-sensei.php',$active_plugins )
	       || in_array( 'sensei/woothemes-sensei.php', $active_plugins )
	       || array_key_exists( 'sensei/woothemes-sensei.php', $active_plugins );

}

function skilled_is_learnpress_active() {
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	if ( is_plugin_active( 'learnpress/learnpress.php' )  || is_plugin_active( 'LearnPress/learnpress.php' )) {
		return true;
	}

	return false;
}

/**
 * Place a cart icon with number of items and total cost in the menu bar.
 *
 * Source: http://wordpress.org/plugins/woocommerce-menu-bar-cart/
 */
function skilled_wcmenucart($menu, $args) {

	// Check if WooCommerce is active and add a new item to a menu assigned to Primary Navigation Menu location
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || 'primary_navigation' !== $args->theme_location )
		return $menu;

	ob_start();
	global $woocommerce;
	$viewing_cart = esc_html__('View your shopping cart', 'skilled');
	$start_shopping = esc_html__('Start shopping', 'skilled');
	$cart_url = wc_get_cart_url();
	$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
	$cart_contents_count = $woocommerce->cart->cart_contents_count;
//	$cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'wheels'), $cart_contents_count);
	$cart_contents = sprintf(_n('%d', '%d', $cart_contents_count, 'skilled'), $cart_contents_count);
	$cart_total = $woocommerce->cart->get_cart_total();
	$menu_item = '';
	// Uncomment the line below to hide nav menu cart item when there are no items in the cart
	 if ( $cart_contents_count > 0 ) {
		if ($cart_contents_count == 0) {
			$menu_item = '<li class="menu-item"><a class="wcmenucart-contents" href="'. $shop_page_url .'" title="'. $start_shopping .'">';
		} else {
			$menu_item = '<li class="menu-item"><a class="wcmenucart-contents" href="'. $cart_url .'" title="'. $viewing_cart .'">';
		}

		$menu_item .= '<i class="fa fa-shopping-cart"></i> ';

		$menu_item .= $cart_contents.' - '. $cart_total;
		$menu_item .= '</a></li>';
	// Uncomment the line below to hide nav menu cart item when there are no items in the cart
	 }
	echo '' . $menu_item;
	$social = ob_get_clean();
	return $menu . $social;

}


function skilled_register_custom_thumbnail_sizes() {
	$string = skilled_get_option( 'custom-thumbnail-sizes' );

	if ( $string ) {

		$pattern     = '/[^a-zA-Z0-9\-\|\:]/';
		$replacement = '';
		$string      = preg_replace( $pattern, $replacement, $string );

		$resArr = explode( '|', $string );
		$thumbs = array();

		foreach ( $resArr as $thumbString ) {
			if ( ! empty( $thumbString ) ) {
				$parts               = explode( ':', trim( $thumbString ) );
				$thumbs[ $parts[0] ] = explode( 'x', $parts[1] );
			}
		}

		foreach ( $thumbs as $name => $sizes ) {
			add_image_size( $name, (int) $sizes[0], (int) $sizes[1], true );
		}
	}
}


if ( ! function_exists( 'skilled_entry_meta' ) ) {

	/**
	 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
	 *
	 * @return void
	 */
	function skilled_entry_meta() {
		if ( is_sticky() && is_home() && ! is_paged() ) {
			echo '<span class="featured-post">' . esc_html__( 'Sticky', 'skilled' ) . '</span>';
		}

		if ( ! has_post_format( 'link' ) && 'post' == get_post_type() ) {
			skilled_entry_date();
		}

		// Post author
		if ( 'post' == get_post_type() ) {
			printf( '<i class="lnr lnr-user"></i><span class="author vcard">%1$s <a class="url fn n" href="%2$s" title="%3$s" rel="author">%4$s</a></span>', esc_html__( 'Posted by', 'skilled' ), esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( __( 'View all posts by %s', 'skilled' ), get_the_author() ) ), get_the_author() );

			$num_comments = get_comments_number(); // get_comments_number returns only a numeric value

			if ( $num_comments == 0 ) {

			} else {

				if ( $num_comments > 1 ) {
					$comments = $num_comments . __( ' Comments', 'skilled' );
				} else {
					$comments = __( '1 Comment', 'skilled' );
				}
				echo '<span class="comments-count"><i class="fa fa-comment-o"></i><a href="' . get_comments_link() . '">' . get_comments_number() . '</a></span>';
			}

		}

		// Translators: used between list items, there is a space after the comma.
		$categories_list = get_the_category_list( __( ', ', 'skilled' ) );
		if ( $categories_list ) {
			echo '<i class="lnr lnr-flag"></i><span class="categories-links">' . $categories_list . '</span>';
		}

		// Translators: used between list items, there is a space after the comma.
		$tag_list = get_the_tag_list( '', __( ', ', 'skilled' ) );
		if ( $tag_list ) {
			echo '<i class="fa fa-tag"></i><span class="tags-links">' . $tag_list . '</span>';
		}


	}
}

if ( ! function_exists( 'skilled_entry_date' ) ) {

	/**
	 * Prints HTML with date information for current post.
	 *
	 * @param boolean $echo Whether to echo the date. Default true.
	 *
	 * @return string The HTML-formatted post date.
	 */
	function skilled_entry_date( $echo = true ) {
		if ( has_post_format( array( 'chat', 'status' ) ) ) {
			$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'skilled' );
		} else {
			$format_prefix = '%2$s';
		}

		$date = sprintf( '<span class="date"><i class="lnr lnr-clock"></i><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>', esc_url( get_permalink() ),
			esc_attr( sprintf( esc_html__( 'Permalink to %s', 'skilled' ), the_title_attribute( 'echo=0' ) ) ), esc_attr( get_the_date( 'c' ) ), esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) ) );

		if ( $echo ) {
			echo $date;
		}

		return $date;
	}

}


function skilled_breadcrumb_trail_labels($labels) {

	return wp_parse_args(array(
		'browse'              => esc_html__( 'Browse:',                               'skilled' ),
		'aria_label'          => esc_attr_x( 'Breadcrumbs', 'breadcrumbs aria label', 'skilled' ),
		'home'                => esc_html__( 'Home',                                  'skilled' ),
		'error_404'           => esc_html__( '404 Not Found',                         'skilled' ),
		'archives'            => esc_html__( 'Archives',                              'skilled' ),
		// Translators: %s is the search query. The HTML entities are opening and closing curly quotes.
		'search'              => esc_html__( 'Search results for &#8220;%s&#8221;',   'skilled' ),
		// Translators: %s is the page number.
		'paged'               => esc_html__( 'Page %s',                               'skilled' ),
		// Translators: Minute archive title. %s is the minute time format.
		'archive_minute'      => esc_html__( 'Minute %s',                             'skilled' ),
		// Translators: Weekly archive title. %s is the week date format.
		'archive_week'        => esc_html__( 'Week %s',                               'skilled' ),
	), $labels);

}