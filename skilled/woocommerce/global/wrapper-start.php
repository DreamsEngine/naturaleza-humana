<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

echo get_template_part( 'templates/title' ) . '<div class="'. skilled_class('container').'"><div class="'. skilled_class('content-fullwidth').'">';
?>
